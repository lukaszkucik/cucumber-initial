package core;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;


public class DriverFactory {

    private static RemoteWebDriver driver = null;

    private DriverFactory() throws MalformedURLException {
    }

    private static RemoteWebDriver createNewDriverInstance() throws MalformedURLException {
        RemoteWebDriver driver = null;

        String browser = System.getProperty("browser", "chrome");
        if (browser.equalsIgnoreCase("chrome")) {

            ChromeOptions options = new ChromeOptions();
            options.addArguments("start-maximized");
            DesiredCapabilities dc = DesiredCapabilities.chrome();
            dc.setCapability(ChromeOptions.CAPABILITY, options);

            driver = createRemoteDriver(dc);
        }
        return driver;
    }

    private static RemoteWebDriver createRemoteDriver(DesiredCapabilities cap) throws MalformedURLException {
        return new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), cap);
    }

    public static RemoteWebDriver getDriver() throws MalformedURLException {
        if (driver == null) {
            driver = createNewDriverInstance();
        }
        return driver;
    }

    public static void killDriver() {
        driver.quit();
    }
}