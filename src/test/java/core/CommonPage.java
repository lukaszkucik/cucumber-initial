package core;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;


public class CommonPage {

    protected CommonPage() throws MalformedURLException {
        PageFactory.initElements(DriverFactory.getDriver(), this);
    }

    protected boolean isDisplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    protected void click(WebElement element) {
        waitForElementToBeClickable(element, 15);
        element.click();
    }

    protected String getElementText(WebElement element) {
        waitForVisibilityOfElementLocated(element, 15);
        return element.getText();
    }

    protected void sendKeys(WebElement element, String value) {
        waitForVisibilityOfElementLocated(element, 15);
        element.sendKeys(value);
    }

    private void waitForVisibilityOfElementLocated(WebElement locator, int time) {
        try {
            new WebDriverWait(DriverFactory.getDriver(), time).until(ExpectedConditions.visibilityOf((locator)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void waitForElementToBeClickable(WebElement locator, int time) {
        try {
            new WebDriverWait(DriverFactory.getDriver(), time).until(ExpectedConditions.elementToBeClickable((locator)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}