package core;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/features/",
        format = {"json:target/cucumber.json"},
        tags = {"@Search"},
        plugin = {"pretty", "html:target/report"},
        glue = {"stepdefinitions"})

public class RunFeatures {
}
