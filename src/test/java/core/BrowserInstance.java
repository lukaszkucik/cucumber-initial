package core;

import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.remote.html5.RemoteLocalStorage;


public class BrowserInstance {

    private static RemoteWebDriver driver;
    private static RemoteLocalStorage localStorage;

    public static void setDriver(RemoteWebDriver driver) {
        BrowserInstance.driver = driver;
    }

    public static void navigateTo(String url) {
        driver.get(url);
    }

    public static void initLocalStorage() {
        RemoteExecuteMethod executeMethod = new RemoteExecuteMethod(driver);
        localStorage = new RemoteLocalStorage(executeMethod);
    }

    public static void setSessionStorageItem(String key, String val){
        localStorage.setItem(key, val);
    }
}