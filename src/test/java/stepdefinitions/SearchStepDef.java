package stepdefinitions;

import core.BrowserInstance;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;

public class SearchStepDef {

    @Given("^I navigate to \"([^\"]*)\"$")
    public void iNavigateTo(String arg0) throws Throwable {
        BrowserInstance.navigateTo(arg0);
    }

    @And("^set the session storage item$")
    public void setTheSessionStorageItem() throws Throwable {
        BrowserInstance.setSessionStorageItem("test", "123");
    }
}
