package stepdefinitions;

import core.DriverFactory;
import core.BrowserInstance;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;


public class Hooks {

    @Before
    public void before() throws InterruptedException, MalformedURLException {
        RemoteWebDriver driver = DriverFactory.getDriver();
        BrowserInstance.setDriver(driver);
        BrowserInstance.initLocalStorage();
    }

    @After
    public void after() {
        DriverFactory.killDriver();
    }
}