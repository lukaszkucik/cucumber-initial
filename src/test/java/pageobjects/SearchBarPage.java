package pageobjects;

import core.CommonPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.net.MalformedURLException;

public class SearchBarPage extends CommonPage {

    @FindBy(name = "location")
    private WebElement location;

    public SearchBarPage() throws MalformedURLException {
    }
}