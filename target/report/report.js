$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Search.feature");
formatter.feature({
  "line": 1,
  "name": "Test related to search bar for airbnb",
  "description": "",
  "id": "test-related-to-search-bar-for-airbnb",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4846767186,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Search for a location with given criteria",
  "description": "",
  "id": "test-related-to-search-bar-for-airbnb;search-for-a-location-with-given-criteria",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Search"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I navigate to \"http://airbnb.pl/\"",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "set the session storage item",
  "keyword": "And "
});
formatter.match({
  "arguments": [
    {
      "val": "http://airbnb.pl/",
      "offset": 15
    }
  ],
  "location": "SearchStepDef.iNavigateTo(String)"
});
formatter.result({
  "duration": 8715850129,
  "status": "passed"
});
formatter.match({
  "location": "SearchStepDef.setTheSessionStorageItem()"
});
formatter.result({
  "duration": 34567243,
  "status": "passed"
});
formatter.after({
  "duration": 1570729285,
  "status": "passed"
});
});